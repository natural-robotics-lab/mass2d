direction_order = ['south', 'west', 'north', 'east']
movement_list = [(0, -1), (-1, 0), (0, 1), (1, 0)]


class Module(object):
    """Class for all types of Module"""

    global_identifier = 1

    def __init__(self, coordinates):
        self.identifier = Module.global_identifier
        Module.global_identifier = Module.global_identifier + 1
        self._coordinates = coordinates
        self._type = None
        self.set_neighbour_coordinates()
        self._active = False

    def set_neighbour_coordinates(self):
        self.neighbour_coordinates = {}
        self.neighbour_coordinates['south'] = (self._coordinates[0], self._coordinates[1]-1)
        self.neighbour_coordinates['south-west'] = (self._coordinates[0]-1, self._coordinates[1]-1)
        self.neighbour_coordinates['west'] = (self._coordinates[0]-1, self._coordinates[1])
        self.neighbour_coordinates['north-west'] = (self._coordinates[0]-1, self._coordinates[1]+1)
        self.neighbour_coordinates['north'] = (self._coordinates[0], self._coordinates[1]+1)
        self.neighbour_coordinates['north-east'] = (self._coordinates[0]+1, self._coordinates[1]+1)
        self.neighbour_coordinates['east'] = (self._coordinates[0]+1, self._coordinates[1])
        self.neighbour_coordinates['south-east'] = (self._coordinates[0]+1, self._coordinates[1]-1)

    def get_x_coord(self):
        return self._coordinates[0]

    def get_z_coord(self):
        return self._coordinates[1]

    def get_coordinates(self):
        return self._coordinates

    def set_coordinates(self, coordinates):
        self._coordinates = coordinates
        self.set_neighbour_coordinates()

    def get_type(self):
        return self._type

    def get_neighbour_coordinates(self):
        return self.neighbour_coordinates

    def is_active(self):
        return self._active


class Included_Module(Module):
    """Modules that are stationary in the structure"""

    def __init__(self, coordinates):
        super(Included_Module, self).__init__(coordinates)
        self._type = 0


class Excluded_Module(Module):
    """Modules to be removed"""

    def __init__(self, coordinates):
        super(Excluded_Module, self).__init__(coordinates)
        self._type = 1
        self._removal_order_index = 0
        self.__last_direction = 1

    def get_removal_order_index(self):
        return self._removal_order_index

    def set_removal_order_index(self, order_index):
        self._removal_order_index = order_index

    def make_active(self):
        self._type = 2
        self._active = True

    def set_last_direction(self, last_direction):
        self.__last_direction = last_direction

    def get_last_direction(self):
        return self.__last_direction

    def move(self, configuration):
        # Return True if at sink, False if not.
        if not self._active:
            print('Not yet active, cannot move')
            return False
        if configuration.module_at_coord(self.neighbour_coordinates[direction_order[(self.__last_direction - 1) % 4]]) == None and \
                self.neighbour_coordinates[direction_order[(self.__last_direction - 1) % 4]][1] >= 0 and \
                self.neighbour_coordinates[direction_order[(self.__last_direction - 1) % 4]][1] <= configuration.get_height() and \
                self.neighbour_coordinates[direction_order[(self.__last_direction - 1) % 4]][0] >= -1 and \
                self.neighbour_coordinates[direction_order[(self.__last_direction - 1) % 4]][0] <= configuration.get_width():
            self.set_last_direction((self.__last_direction - 1) % 4)
        elif configuration.module_at_coord(self.neighbour_coordinates[direction_order[self.__last_direction]]) == None and \
                self.neighbour_coordinates[direction_order[self.__last_direction]][1] >= 0 and \
                self.neighbour_coordinates[direction_order[self.__last_direction]][1] <= configuration.get_height() and \
                self.neighbour_coordinates[direction_order[self.__last_direction]][0] >= -1 and \
                self.neighbour_coordinates[direction_order[self.__last_direction]][0] <= configuration.get_width():
            self.set_last_direction(self.__last_direction)
        elif configuration.module_at_coord(self.neighbour_coordinates[direction_order[(self.__last_direction + 1) % 4]]) == None and \
                self.neighbour_coordinates[direction_order[(self.__last_direction + 1) % 4]][1] >= 0 and \
                self.neighbour_coordinates[direction_order[(self.__last_direction + 1) % 4]][1] <= configuration.get_height() and \
                self.neighbour_coordinates[direction_order[(self.__last_direction + 1) % 4]][0] >= -1 and \
                self.neighbour_coordinates[direction_order[(self.__last_direction + 1) % 4]][0] <= configuration.get_width():
            self.set_last_direction((self.__last_direction + 1) % 4)
        else:
            self.set_last_direction((self.__last_direction - 2) % 4)

        new_x_coord = self._coordinates[0] + movement_list[self.__last_direction][0]
        new_z_coord = self._coordinates[1] + movement_list[self.__last_direction][1]
        self.set_coordinates((new_x_coord, new_z_coord))
        if self._coordinates == (-1, 0):
            return True
        else:
            return False
