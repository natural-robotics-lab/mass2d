from configuration import Configuration
import analyse
import pickle
import os
import argparse


def random_config_generator(config_width, config_height, n_excluded_modules, n_configurations=1):
    """Generate a number of random configurations for the given dimensions"""
    if n_excluded_modules > config_width * config_height:
        print('ERROR: More excluded modules than modules in configuration\nMAX EXCLUDED MODULES = %d' % (config_width * config_height))
        exit()

    configuration_directory = './configurations/batch_configs/%d-%d_%d/' % (config_width, config_height, n_excluded_modules)
    if not os.path.exists(configuration_directory):
        os.makedirs(configuration_directory)
        n_existing_configurations = 0
    else:
        n_existing_configurations = len([1 for x in list(os.scandir(configuration_directory)) if x.is_file()])

    for i in range(n_existing_configurations, n_configurations):
        print('Generating %d of %d' % (i+1, n_configurations))
        file_name = configuration_directory + '%.4d.pkl' % len([1 for x in list(os.scandir(configuration_directory)) if x.is_file()])

        random_configuration = Configuration(config_width, config_height, random_configuration=True,
                                             n_excluded_modules=n_excluded_modules)
        random_configuration.save_configuration(file_name)


def batch_simulator(config_width, config_height, n_excluded_modules, save_vis=False):
    """Simulate the configurations that exist for the given dimensions"""
    configuration_directory = './configurations/batch_configs/%d-%d_%d/' % (config_width, config_height, n_excluded_modules)
    if not os.path.exists(configuration_directory):
        print('ERROR: No configurations in specified directory')
        exit()

    results_directory = './batch_results/%d-%d_%d/' % (config_width, config_height, n_excluded_modules)
    if not os.path.exists(results_directory):
        os.makedirs(results_directory)
    results_file = results_directory + '%d-%d_%d.txt' % (config_width, config_height, n_excluded_modules)
    if not os.path.exists(results_file):
        open(results_file, 'a').close()
        n_existing_results = 0
    else:
        n_existing_results = len(open(results_file).readlines())

    n_total_results = len([1 for x in list(os.scandir(configuration_directory)) if x.is_file()])

    for i in range(n_existing_results, n_total_results):
        print('Simulating %d of %d' % (i+1, n_total_results))
        file_name = configuration_directory + '%.4d.pkl' % i
        initial_configuration = pickle.load(open(file_name, 'rb'))
        old_sequential_reconfiguration, old_sequential_reconfiguration_time = initial_configuration.full_reconfiguration(
            optimal_ordering=False, parallel_movement=False)
        old_parallel_reconfiguration, old_parallel_reconfiguration_time = initial_configuration.full_reconfiguration(optimal_ordering=False)
        new_sequential_reconfiguration, new_sequential_reconfiguration_time = initial_configuration.full_reconfiguration(parallel_movement=False)
        new_parallel_reconfiguration, new_parallel_reconfiguration_time = initial_configuration.full_reconfiguration()
        bct_list, sequential_bct, parallel_bct = analyse.get_all_best_case_times(initial_configuration)
        with open(results_file, 'a') as file:
            file.write(''.join(str(result).ljust(10) for result in [old_sequential_reconfiguration_time, old_parallel_reconfiguration_time,
                                                                    new_reconfiguration_time, sequential_bct, parallel_bct]))
            # file.write('\n')
            file.write('[%d, %d, %d, %d, %d, %d]\n' % (old_sequential_reconfiguration_time, old_parallel_reconfiguration_time,
                                                       new_sequential_reconfiguration_time, new_parallel_reconfiguration_time,
                                                       sequential_bct, parallel_bct))
        if save_vis == True and i % 100 == 0:
            print('Saving visualisation of results %d' % (i+1))
            old_sequential_reconfiguration.save_visualisation(directory=(
                results_directory + 'old_sequential_visualisations/%.4d/' % i), show_labels=False)
            old_parallel_reconfiguration.save_visualisation(directory=(
                results_directory + 'old_parallel_visualisations/%.4d/' % i), show_labels=False)
            new_sequential_reconfiguration.save_visualisation(directory=(
                results_directory + 'new_sequential_visualitations/%.4d/' % i), show_labels=False)
            new_parallel_reconfiguration.save_visualisation(directory=(
                results_directory + 'new_parallel_visualisations/%.4d/' % i), show_labels=False)
        elif i % 100 == 0:
            old_sequential_reconfiguration.save_configuration(name=results_directory + 'old_sequential_reconfiguration_%.4d.pkl' % i)
            old_parallel_reconfiguration.save_configuration(name=results_directory + 'old_parallel_reconfiguration_%.4d.pkl' % i)
            new_sequential_reconfiguration.save_configuration(name=results_directory + 'new_sequential_reconfiguration_%.4d.pkl' % i)
            new_parallel_reconfiguration.save_configuration(name=results_directory + 'new_parallel_reconfiguration_%.4d.pkl' % i)


parser = argparse.ArgumentParser(description='run batches of simulations in mass2d')

# Compulsory options for dimensions of configurations
parser.add_argument('width', type=int, metavar='width', help='configuration width')
parser.add_argument('height', type=int, metavar='height', help='configuration height')
parser.add_argument('n_modules', type=int, metavar='no. modules', help='the number of excluded modules to randomly assign')

# Optional arguments
parser.add_argument('-c', '--check', action='store_true', help='check number of configurations and results for given values')
parser.add_argument('--configs', type=int, metavar='int', default=100, help='the number of configurations to generate (default: 100)')
parser.add_argument('--generate_only', action='store_true', help='only generate random configurations (don\'t simulate)')
parser.add_argument('--simulate_only', action='store_true', help='only simulate the already generated configurations')
parser.add_argument('-v', '--save_vis', action='store_true', help='save visualisations of every 100th run')

# Parse arguments
args = parser.parse_args()

# If checking status then only do that
if args.check:
    # Check how many configurations have already been generated
    configuration_directory = './configurations/batch_configs/%d-%d_%d/' % (args.width, args.height, args.n_modules)
    if not os.path.exists(configuration_directory):
        n_existing_configurations = 0
    else:
        n_existing_configurations = len([1 for x in list(os.scandir(configuration_directory)) if x.is_file()])
    # Check how many reconfigurations have been simulated
    results_file = './batch_results/%d-%d_%d/%d-%d_%d.txt' % (args.width, args.height, args.n_modules, args.width, args.height, args.n_modules)
    if not os.path.exists(results_file):
        n_existing_results = 0
    else:
        n_existing_results = len(open(results_file).readlines())
    print('%d configurations exist, of which %d have been simulated.' % (n_existing_configurations, n_existing_results))
    exit()

# Generate configurations if required
if not args.simulate_only:
    random_config_generator(args.width, args.height, args.n_modules, args.configs)

# Simulate reconfiguration if required
if not args.generate_only:
    batch_simulator(args.width, args.height, args.n_modules, save_vis=args.save_vis)
