from configuration import Configuration
import pickle
import argparse
import os

parser = argparse.ArgumentParser(description='view configurations and reconfigurations from file')

# Optional arguments
parser.add_argument('-l', '--labels', action='store_true', help='display module priority labels')
parser.add_argument('-i', '--inanimate', action='store_true', help='only display the initial frame')

# Location of file to view
parser.add_argument('file', type=str, metavar='filepath', help='location of configuration file')

args = parser.parse_args()

# Check file exists
if os.path.exists(args.file):
    config = pickle.load(open(args.file, 'rb'))
else:
    print('ERROR: no such configuration exists')
    exit()

# Show inanimate configuration if requested or if no sequence simulated
if args.inanimate or config.get_reconfiguration_time() == -2:
    config.visualise_current(show_labels=args.labels)
else:
    config.visualise_sequence(show_labels=args.labels)
