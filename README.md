# MASS2D - Modular Active Subtraction Simulator in 2D
![coverimage](reconfigurations/examples/30x10_NRL/_animation.gif)

A command line interface for simulating active subtraction using modular robots in 2 dimensions.
Consists of files for general use, along with specifically for viewing configurations and batch generation/simulation.
Written and tested in Python 3.8.

## Installation
There are a few requirements, most easily installed with [pip](https://pip.pypa.io/en/stable/):

```bash
pip install numpy matplotlib seaborn imageio scikit-image pathfinding
```

Then clone the repository and use from there.

```bash
git clone -b master --single-branch https://gitlab.com/natural-robotics-lab/mass2d.git
```

## Usage
### MASS2D
For general usage use mass2d.py:

```bash
python mass2d.py -h
```

MASS2D allows the interactive creation of desired configurations:

```bash
python mass2d.py new -h
```

or loading a configuration from file:

```bash
python mass2d.py load -h
```

or the generation of a random configuration from a set of given parameters:

```bash
python mass2d.py random -h
```

### Viewer
For viewing configurations/reconfiguration sequences use viewer.py:

```bash
python viewer.py -h
```

### Batch Simulations
If you need many configurations and simulations use batch_simulations.py:

```bash
python batch_simulations.py -h
```

## Examples
A number of example configurations are included in the directory `configurations/examples/`.
They can be viewed or simulated to test the program.

```bash
python viewer.py ./configurations/examples/8x8_cavity_example.pkl # view configuration before simulating
python mass2d.py load ./configurations/examples/8x8_cavity_example.pkl # simulate reconfiguration with default options
```

## License
[MIT](./LICENSE)
