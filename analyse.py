from configuration import Configuration
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
import numpy as np


def get_best_case_time(configuration: Configuration, x_coord, z_coord):
    if configuration.module_at_coord((x_coord, z_coord)).get_type() == 0:
        print('Module is included and cannot move')
        return 0

    matrix = np.ones(shape=(configuration.get_width()+2, configuration.get_height()+1))
    for x in range(0, configuration.get_width()):
        for z in range(0, configuration.get_height()):
            if configuration.module_at_coord((x, z)).get_type() == 0:
                matrix[x+1][z] = 0
    grid = Grid(matrix=matrix.T)

    start = grid.node(x_coord+1, z_coord)
    sink = grid.node(0, 0)

    finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, sink, grid)

    return len(path)-1


def get_all_best_case_times(configuration: Configuration):
    bct_list = []
    for x in range(0, configuration.get_width()):
        for z in range(0, configuration.get_height()):
            if configuration.module_at_coord((x, z)).get_type() == 1:
                bct_list.append(get_best_case_time(configuration, x, z))
    sequential_bct = sum(bct_list)
    sorted_list = bct_list
    sorted_list.sort()
    parallel_bct = min(sorted_list)
    for i in range(1, len(sorted_list)):
        if parallel_bct + 2 > sorted_list[i]:
            parallel_bct = parallel_bct+2
        else:
            parallel_bct = sorted_list[i]
    return bct_list, sequential_bct, parallel_bct
