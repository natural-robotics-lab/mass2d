from configuration import Configuration
import pickle
import argparse
import analyse

parser = argparse.ArgumentParser(description='mass2d command line interface')

# Option for the ordering of excluded modules
parser.add_argument('-o', '--order', type=str, choices=['optimal', 'suboptimal'],
                    action='append', default=[], help='choose the method for ordering excluded modules')

parser.add_argument('-q', '--sequential', action='store_true', help='use sequential movement (default parallel)')

# Optional arguments
parser.add_argument('-v', '--hidevis', action='store_true', help='do not display the configuration once generated/simulated')
parser.add_argument('-l', '--labels', action='store_true', help='show labels on visualisations')
parser.add_argument('-s', '--save', action='store_true', help='save the reconfiguration')

# Mutually exclusive methods for choosing configuration
config_subparsers = parser.add_subparsers(help='configuration creation options')

# Options for loading configuration from file
load_parser = config_subparsers.add_parser('load', help='load a configuration from file')
load_parser.add_argument('file', type=str, metavar='filepath', help='location of configuration file')

# Options for creating new configuration
new_parser = config_subparsers.add_parser('new', help='create a new configuration')
new_parser.add_argument('width', type=int, metavar='width', help='configuration width')
new_parser.add_argument('height', type=int, metavar='height', help='configuration height')
new_parser.add_argument('-c', '--saveconfig', type=str, metavar='path',
                        help='specify where to save the configuration file (if desired)')

# Options for generating random configuration
rand_parser = config_subparsers.add_parser('random', help='randomly generate a configuration')
rand_parser.add_argument('width', type=int, metavar='width', help='configuration width')
rand_parser.add_argument('height', type=int, metavar='height', help='configuration height')
rand_parser.add_argument('n_modules', type=int, metavar='no. modules',
                         help='specify the number of excluded modules to randomly assign')
rand_parser.add_argument('-c', '--saveconfig', type=str, metavar='path',
                         help='specify where to save the configuration file (if desired)')

# Parse arguments
args = parser.parse_args()

# Load/create configuration
if hasattr(args, 'file'):
    initial_configuration = pickle.load(open(args.file, 'rb'))
elif hasattr(args, 'n_modules'):
    initial_configuration = Configuration(args.width, args.height, random_configuration=True, n_excluded_modules=args.n_modules)
    if not args.hidevis:
        initial_configuration.visualise_current(show_labels=args.labels)
else:
    initial_configuration = Configuration(args.width, args.height)

# Save configuration before simulating
if hasattr(args, 'saveconfig'):
    initial_configuration.save_configuration(args.saveconfig)

if not args.order:
    args.order = ['optimal']

# Simulate reconfiguration for given configuration
reconfiguration_time_list = []
for solver_type in args.order:
    if initial_configuration.get_reconfiguration_time() > -2:
        print('Configuration has already been simulated (your options may not be applied)')
        reconfiguration = initial_configuration
        reconfiguration_time = reconfiguration.get_reconfiguration_time()
    else:
        if solver_type == 'optimal':
            reconfiguration, reconfiguration_time = initial_configuration.full_reconfiguration(parallel_movement=not args.sequential)
        elif solver_type == 'suboptimal':
            reconfiguration, reconfiguration_time = initial_configuration.full_reconfiguration(
                optimal_ordering=False, parallel_movement=not args.sequential)
    if not args.hidevis:
        reconfiguration.visualise_sequence(show_labels=args.labels)
    if args.save:
        reconfiguration.save_visualisation(show_labels=args.labels)
    reconfiguration_time_list.append((solver_type, reconfiguration_time))

# Output the reconfiguration time
if args.sequential:
    move_type = 'sequential'
else:
    move_type = 'parallel'
for t in reconfiguration_time_list:
    print('%s ordering %s reconfiguration time: \t%d' % (t[0], move_type, t[1]))

# Analyse best case scenario performance
bct_list, sequential_bct, parallel_bct = analyse.get_all_best_case_times(initial_configuration)
print('best case sequential reconfiguration time:\t\t%d' % sequential_bct)
print('best case parallel reconfiguration time:\t\t%d' % parallel_bct)
