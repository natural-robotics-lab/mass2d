from module import Excluded_Module
from module import Included_Module
import matplotlib.pyplot as P
import numpy as np
import seaborn as sns
from matplotlib.colors import ListedColormap as lcm
from matplotlib.widgets import Button, Slider
import copy
import time
import os
import imageio
import pickle
import random
from skimage import morphology

# Set parameters for all plots
P.style.use('seaborn-pastel')
P.rcParams.update({
    'xtick.bottom': False,
    'xtick.labelbottom': False,
    'xtick.top': False,
    'xtick.labeltop': False,
    'ytick.left': False,
    'ytick.labelleft': False,
    'ytick.right': False,
    'ytick.labelright': False})
cmap = lcm(['white', *[sns.cubehelix_palette(8)[x] + [1, ] for x in [5, 3, 0]]])

manhattan_neighbourhood = ['south', 'west', 'north', 'east']
manhattan_coord_modifiers = [(0, -1), (-1, 0), (0, 1), (1, 0)]
moore_neighbourhood = ['south', 'south-west', 'west', 'north-west', 'north', 'north-east', 'east', 'south-east']


class Configuration(object):
    """Create a configuration of modules, of specified width and height"""

    def __init__(self, __width, __height, random_configuration=False, n_excluded_modules=0):
        self.removal_order_index = 1
        self.__width = __width
        self.__height = __height
        self.modules = []
        self.__module_removal_order = []
        self.__reconfiguration_sequence = []

        # Initialise with all module coordinates as included and none excluded
        included_module_coords = []
        excluded_module_coords = []
        for x in range(0, self.__width):
            for z in range(0, self.__height):
                included_module_coords.append((x, z))

        if random_configuration == False:   # Use GUI to determine configuration
            module_matrix = np.ones((self.__width, self.__height), dtype=int)
            # Initialise plot
            fig, ax = P.subplots()
            data = ax.matshow(module_matrix.T, origin='lower', cmap=lcm(cmap.colors[::-1]), vmin=0, vmax=2)
            x_ticks = [i - 0.5 for i in range(self.__width)]
            z_ticks = [i - 0.5 for i in range(self.__height)]
            P.xticks(x_ticks, [])
            P.yticks(z_ticks, [])
            ax.tick_params(bottom=False, top=False, left=False, right=False)
            ax.grid(b=True, which='both', color='w')
            ax.set_title('Select the modules to be removed:')
            # Buttons for confirmation or cancelling
            confirmax = P.axes([0.88, 0.18, 0.1, 0.05])
            confirmbutton = Button(confirmax, 'Confirm', color='lightgreen', hovercolor='0.975')
            cancelax = P.axes([0.88, 0.11, 0.1, 0.05])
            cancelbutton = Button(cancelax, 'Cancel', color='red', hovercolor='0.975')
            # Define click events for module selection and confirmation

            def onclick(event):
                if event.inaxes == ax:
                    global iX, iZ
                    iX, iZ = int(round(event.xdata)), int(round(event.ydata))
                    if included_module_coords.count((iX, iZ)) == 0:
                        module_matrix[iX, iZ] = 1
                        included_module_coords.append((iX, iZ))
                        excluded_module_coords.remove((iX, iZ))
                    else:
                        module_matrix[iX, iZ] = 0
                        included_module_coords.remove((iX, iZ))
                        excluded_module_coords.append((iX, iZ))
                    data.set_data(module_matrix.T)
                    fig.canvas.draw_idle()
                if event.inaxes == confirmax:
                    P.close()  # Closes plot, advancing to the end
                if event.inaxes == cancelax:
                    exit()
            cid = fig.canvas.mpl_connect('button_press_event', onclick)

            # Show plot
            P.show()  # Waits for plot to close

        else:   # Randomly assign excluded modules
            connection_status = 0
            while connection_status != 1:
                # Determine the coordinates of modules with free faces
                candidate_coords = []
                for z_coord in range(0, self.__height-1):
                    candidate_coords.append((self.__width-1, z_coord))
                for x_coord in range(self.__width-1, -1, -1):
                    candidate_coords.append((x_coord, self.__height-1))
                for z_coord in range(self.__height-2, -1, -1):
                    candidate_coords.append((0, z_coord))

                # Choose as many excluded coods as designated by n_excluded_modules
                while len(excluded_module_coords) < n_excluded_modules:
                    # Make sure that if there is only one included grounded module it cannot be selected
                    n_grounded_included_modules = 0
                    for included_coord in included_module_coords:
                        if included_coord[1] == 0:
                            n_grounded_included_modules = n_grounded_included_modules + 1
                            grounded_included_module = included_coord
                    if n_grounded_included_modules == 1 and grounded_included_module in candidate_coords:
                        candidate_coords.remove(grounded_included_module)
                    # Select new_coord to be added to list of excluded coords
                    new_coord = None
                    while new_coord == None:
                        # Choose possible coordinate
                        candidate_coord = random.choice(candidate_coords)
                        # Check configuration would be connected
                        grid = np.ones((self.__width, self.__height+1), dtype=int)
                        for x, z in excluded_module_coords:
                            grid[x, z+1] = 0
                        grid[candidate_coord[0], candidate_coord[1]+1] = 0
                        connection_grid, connection_status = morphology.label(grid, background=0, return_num=True, connectivity=1)
                        if connection_status == 1:
                            new_coord = candidate_coord

                    excluded_module_coords.append(new_coord)
                    included_module_coords.remove(new_coord)
                    candidate_coords.remove(new_coord)
                    for neighbour_coord_modifier in manhattan_coord_modifiers:
                        neighbour_coord = tuple(np.add(new_coord, neighbour_coord_modifier))
                        if neighbour_coord in included_module_coords and not neighbour_coord in candidate_coords:
                            candidate_coords.append(neighbour_coord)

        # Create list of modules with properties given by user/determined randomly
        for i_included_modules in included_module_coords:
            self.modules.append(Included_Module(i_included_modules))
        for i_excluded_modules in excluded_module_coords:
            self.modules.append(Excluded_Module(i_excluded_modules))

    def full_reconfiguration(self, optimal_ordering=True, parallel_movement=True):
        reconfiguration_configuration = copy.deepcopy(self)
        if optimal_ordering:
            reconfiguration_configuration.assign_optimal_removal_order_indexes()
        else:
            reconfiguration_configuration.assign_suboptimal_removal_order_indexes()
        if parallel_movement and optimal_ordering:
            activation_times = reconfiguration_configuration.get_parallel_module_activation_times()
        elif parallel_movement and not optimal_ordering:
            activation_times = reconfiguration_configuration.get_parallel_module_activation_times_collision_check()
        else:
            activation_times = reconfiguration_configuration.get_sequential_module_activation_times()
        reconfiguration_configuration.remove_excluded_modules(activation_times)
        reconfiguration_time = reconfiguration_configuration.get_reconfiguration_time()

        return reconfiguration_configuration, reconfiguration_time

    def save_configuration(self, name=None):
        if not name:
            current_seconds = time.time()
            time_struct = time.localtime(current_seconds)
            name = './configurations/%d-%.2d-%.2d_%.2d-%.2d.pkl' % (time_struct.tm_year, time_struct.tm_mon, time_struct.tm_mday,
                                                                    time_struct.tm_hour, time_struct.tm_min)
        print('Saving configuration to ' + name)
        with open(name, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    def get_width(self):
        return self.__width

    def get_height(self):
        return self.__height

    def get_n_excluded_modules(self):
        n_exc = 0
        for i_module in self.modules:
            if i_module.get_type() == 1:
                n_exc = n_exc + 1
        return n_exc

    def get_reconfiguration_time(self):
        return len(self.__reconfiguration_sequence)-2

    def configuration_to_matrix(self):
        module_matrix = np.ndarray((self.__width+2, self.__height+1), dtype=int)
        module_matrix.fill(-1)
        orderi_matrix = np.ndarray((self.__width+2, self.__height+1), dtype=int)
        orderi_matrix.fill(-1)
        for i_module in self.modules:
            coords = i_module.get_coordinates()
            module_matrix[coords[0]+1, coords[1]] = i_module.get_type()
            if type(i_module) is Excluded_Module:
                orderi_matrix[coords[0]+1, coords[1]] = i_module.get_removal_order_index()
        return module_matrix.T, orderi_matrix.T

    def visualise_current(self, show_labels=True):
        fig, ax = P.subplots()
        module_matrix, orderi_matrix = self.configuration_to_matrix()
        if show_labels:
            for i_z, row in enumerate(orderi_matrix):
                for i_x, value in enumerate(row):
                    if value != -1:
                        ax.text(i_x+0.5, i_z+0.5, value, va='center', ha='center')
        ax.pcolor(module_matrix, cmap=cmap, edgecolors='w', linewidths=2, vmin=-1, vmax=2)
        ax.set_aspect('equal')
        P.show()

    def visualise_sequence(self, show_labels=True):
        current_seconds = time.time()
        time_struct = time.localtime(current_seconds)
        current_time = '%d-%.2d-%.2d_%.2d-%.2d' % (time_struct.tm_year, time_struct.tm_mon, time_struct.tm_mday,
                                                   time_struct.tm_hour, time_struct.tm_min)
        fig, ax = P.subplots()

        module_matrix, orderi_matrix = self.__reconfiguration_sequence[0]
        if show_labels:
            for i_z, row in enumerate(orderi_matrix):
                for i_x, value in enumerate(row):
                    if value != -1:
                        ax.text(i_x+0.5, i_z+0.5, value, va='center', ha='center')
        ax.pcolor(module_matrix, cmap=cmap, edgecolors='w', linewidths=2, vmin=-1, vmax=2)
        ax.set_aspect('equal')

        close_axis = P.axes([0.92, 0.11, 0.06, 0.05])
        close_button = Button(close_axis, 'Close', color='red', hovercolor='0.975')

        def close(event):
            P.close()

        close_button.on_clicked(close)

        save_axis = P.axes([0.92, 0.18, 0.06, 0.05])
        save_button = Button(save_axis, 'Save', color='lightgreen', hovercolor='0.975')

        def save(event):
            P.close()
            self.save_visualisation()

        save_button.on_clicked(save)

        save_current_axis = P.axes([0.92, 0.25, 0.06, 0.05])
        save_current_button = Button(save_current_axis, 'Save\nCurrent', color='lightgreen', hovercolor='0.975')

        def save_current(event):
            directory = './snapshots/'+current_time
            if not os.path.exists(directory):
                os.makedirs(directory)
            print('Saving frame %d to ' % int(slider_values.val)+directory)
            filename = (directory + '/frame-%.4d.png' % slider_values.val)
            configonly = ax.get_tightbbox(fig.canvas.renderer).transformed(fig.dpi_scale_trans.inverted())
            fig.savefig(filename, dpi=1000, bbox_inches=configonly)

        save_current_button.on_clicked(save_current)

        slider_axis = P.axes([0.34, 0.02, 0.40, 0.05], facecolor='white')
        slider_values = Slider(slider_axis, 'Step', valmin=0, valmax=len(self.__reconfiguration_sequence)-1, valstep=1, valfmt='%d')

        def update(val):
            slider_reading = int(slider_values.val)
            module_matrix, orderi_matrix = self.__reconfiguration_sequence[slider_reading]
            if show_labels:
                for text in ax.texts:
                    text.set_visible(False)
                for text in ax.texts:
                    text.remove()
                for i_z, row in enumerate(orderi_matrix):
                    for i_x, value in enumerate(row):
                        if value != -1:
                            ax.text(i_x+0.5, i_z+0.5, value, va='center', ha='center')
                        else:
                            ax.text(i_x+0.5, i_z+0.5, '', va='center', ha='center')
            ax.pcolor(module_matrix, cmap=cmap, edgecolors='w', linewidths=2, vmin=-1, vmax=2)
            fig.canvas.draw_idle()

        slider_values.on_changed(update)

        def press(event):
            if event.key == 'right':
                if int(slider_values.val) < slider_values.valmax:
                    slider_values.set_val(slider_values.val + 1)
            elif event.key == 'left':
                if int(slider_values.val) > 0:
                    slider_values.set_val(slider_values.val - 1)

        fig.canvas.mpl_connect('key_press_event', press)

        P.show()

    def save_visualisation(self, directory=None, show_labels=False):
        images = []
        if directory == None:
            current_seconds = time.time()
            time_struct = time.localtime(current_seconds)
            directory = './reconfigurations/%d-%.2d-%.2d_%.2d-%.2d' % (time_struct.tm_year, time_struct.tm_mon, time_struct.tm_mday,
                                                                       time_struct.tm_hour, time_struct.tm_min)
        while os.path.exists(directory):
            directory = directory + '(2)'
        os.makedirs(directory)
        print('Saving reconfiguration to ' + directory)
        for i_time_step in range(len(self.__reconfiguration_sequence)):
            fig, ax = P.subplots()
            module_matrix, orderi_matrix = self.__reconfiguration_sequence[i_time_step]
            if show_labels:
                for i_z, row in enumerate(orderi_matrix):
                    for i_x, value in enumerate(row):
                        if value != -1:
                            ax.text(i_x+0.5, i_z+0.5, value, va='center', ha='center')
            ax.pcolor(module_matrix, cmap=cmap, edgecolors='w', linewidths=2, vmin=-1, vmax=2)
            ax.set_aspect('equal')
            filename = (directory + '/frame-%.4d.png' % i_time_step)
            P.savefig(filename, dpi=100, bbox_inches='tight')
            images.append(imageio.imread(filename))
            P.close()
        imageio.mimsave((directory + '/_animation.gif'), images, duration=min(0.2, (20/len(self.__reconfiguration_sequence))))
        with open(directory + '/_configuration.pkl', 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    def module_at_coord(self, coordinates):
        for module in self.modules:
            if module.get_coordinates() == coordinates:
                return module
        return None

    def set_removal_order_index(self, module):
        module.set_removal_order_index(self.removal_order_index)
        self.__module_removal_order.append(module)
        self.removal_order_index = self.removal_order_index + 1

    def get_cavity_starting_module(self):
        candidate_module = None
        for external_coord in self.__external_coords:
            external_module = self.module_at_coord(external_coord)
            if candidate_module:
                if external_module.get_type() == 1 and external_module.get_removal_order_index() == 0:
                    candidate_module = external_module
                else:
                    return candidate_module
            else:
                if external_module.get_type() == 1 and external_module.get_removal_order_index() == 0:
                    candidate_module = external_module
        if candidate_module:
            return candidate_module
        else:
            return None

    def get_excluded_modules_connected_to(self, initial_module):
        connected_excluded_modules = [initial_module]
        modules_to_check_neighbours = [initial_module]
        while modules_to_check_neighbours:
            last_module = modules_to_check_neighbours[0]
            neighbours_coordinates = last_module.get_neighbour_coordinates()
            for i_neighbour_direction in manhattan_neighbourhood:
                neighbour_module = self.module_at_coord(neighbours_coordinates[i_neighbour_direction])
                if neighbour_module == None:
                    pass
                else:
                    if neighbour_module.get_type() >= 1 and neighbour_module not in connected_excluded_modules:
                        connected_excluded_modules.append(neighbour_module)
                        if neighbour_module not in modules_to_check_neighbours:
                            modules_to_check_neighbours.append(neighbour_module)
            modules_to_check_neighbours.remove(last_module)
        return connected_excluded_modules

    def assign_optimal_removal_order_indexes(self):
        # Check for full rows of excluded modules at the top
        top_layer = self.__height
        full_excluded_row = True
        while full_excluded_row:
            top_layer = top_layer - 1
            for x_coord in range(self.__width):
                if self.module_at_coord((x_coord, top_layer)).get_type() == 0:
                    full_excluded_row = False
            if full_excluded_row:
                for x_coord in range(self.__width):
                    self.set_removal_order_index(self.module_at_coord((x_coord, top_layer)))
        # Determine the outer shell of modules
        self.__external_coords = []
        for z_coord in range(0, top_layer):
            self.__external_coords.append((self.__width-1, z_coord))
        for x_coord in range(self.__width-1, -1, -1):
            self.__external_coords.append((x_coord, top_layer))
        for z_coord in range(top_layer-1, -1, -1):
            self.__external_coords.append((0, z_coord))
        # Assign order indexes to cavities
        initial_module = self.get_cavity_starting_module()
        while initial_module:
            self.assign_removal_order_indexes_for_cavity(initial_module)
            initial_module = self.get_cavity_starting_module()

    def assign_removal_order_indexes_for_cavity(self, initial_module=None):
        if not initial_module:
            initial_module = self.get_cavity_starting_module()
        checking_module_wait_list = []
        next_module = None

        # Determine initial direction based on which face the initial module is on
        if initial_module.get_x_coord() == self.__width - 1:
            initial_direction = 1
        elif initial_module.get_z_coord() == self.__height - 1:
            initial_direction = 0
        else:
            initial_direction = 3

        waiting_modules = [(initial_module, initial_direction)]

        while waiting_modules:
            # Get last module from list of waiting modules
            if len(waiting_modules) == 1:
                last_module = waiting_modules[-1][0]
                last_direction = waiting_modules[-1][1]
            # Check that the last module does not have any waiting neighbours
            else:
                candidate_position = len(waiting_modules) - 1
                candidate_module = waiting_modules[candidate_position][0]
                candidate_direction = waiting_modules[candidate_position][1]
                while candidate_position:
                    direction_to_check = manhattan_neighbourhood[(candidate_direction + 1) % 4]
                    other_module = self.module_at_coord(candidate_module.get_neighbour_coordinates()[direction_to_check])
                    if (other_module, 0) in waiting_modules or (other_module, 1) in waiting_modules \
                            or (other_module, 2) in waiting_modules or (other_module, 3) in waiting_modules:
                        # print("Neighbour waiting")
                        candidate_position = candidate_position - 1
                        candidate_module = waiting_modules[candidate_position][0]
                        candidate_direction = waiting_modules[candidate_position][1]
                    else:
                        candidate_position = None

                last_module = candidate_module
                last_direction = candidate_direction

            # Remove the module to be assessed from the waiting_modules list
            for i_direction in range(0, 4):
                if (last_module, i_direction) in waiting_modules:
                    waiting_modules.remove((last_module, i_direction))
            # Set the removal order for the module, and assign a priority direction based on order
            self.set_removal_order_index(last_module)
            last_module.set_last_direction((last_direction - 2) % 4)
            # print(str(last_module.get_coordinates()) + " assigned " + str(self.removal_order_index-1) + " by wait list")

            dead_end = False
            # Explore cavity assigning indexes until a dead end
            while not dead_end:
                index_assigned = False
                # Assess neighbours in an order based on the last direction a module was assigned
                directions_to_check = list(
                    map(lambda x: manhattan_neighbourhood[x % 4], np.arange(last_direction-1, last_direction+2, 1)))
                for direction in directions_to_check:
                    if direction == directions_to_check[-1]:
                        index_assigned = True
                    checking_module = self.module_at_coord(last_module.get_neighbour_coordinates()[direction])
                    # Make sure neighbour is valid module
                    if checking_module:
                        # Check module is excluded and not waiting, irrelevant of associated direction
                        if checking_module.get_type() == 1 and checking_module.get_removal_order_index() == 0:
                            # If no module has yet been assigned an order index then do so
                            if not index_assigned:
                                next_module = checking_module
                                last_direction = manhattan_neighbourhood.index(direction)
                                index_assigned = True
                            # If a module has been assigned an order index, add other modules to wait list
                            else:
                                checking_module_wait_list.insert(0, (checking_module, manhattan_neighbourhood.index(direction)))
                # Remove the next_module from waiting_modules if it is in there
                for i_direction in range(0, 4):
                    if (next_module, i_direction) in waiting_modules:
                        waiting_modules.remove((next_module, i_direction))
                # Add the waiting modules from the checking_module to the waiting_modules
                waiting_modules = waiting_modules + checking_module_wait_list
                checking_module_wait_list = []
                # If there is a module to assign an order to, do so
                if next_module:
                    last_module = next_module
                    self.set_removal_order_index(last_module)
                    last_module.set_last_direction((last_direction - 2) % 4)
                    # print(str(next_module.get_coordinates()) + " assigned " +
                    #       str(self.removal_order_index-1) + " by neighbour to the " + manhattan_neighbourhood[last_direction])
                    next_module = None
                # If there is not, it is a dead end and a waiting module should be used
                else:
                    dead_end = True
        return True

    def assign_suboptimal_removal_order_indexes(self):
        # Old method of determining priority
        temp_configuration = copy.deepcopy(self)
        excluded_modules = True
        while excluded_modules:
            excluded_modules = False
            coords = ((i_z, i_x) for i_z in range(self.__height - 1, -1, -1) for i_x in range(0, self.__width))
            for i_z, i_x in coords:
                candidate_module = temp_configuration.module_at_coord((i_x, i_z))
                if candidate_module and candidate_module.get_type() == 1:
                    excluded_modules = True
                    candidate_module_is_free = False
                    neighbours_coordinates = candidate_module.get_neighbour_coordinates()
                    for i_direction in manhattan_neighbourhood:
                        if i_direction == 'south' and i_z == 0:
                            pass
                        elif temp_configuration.module_at_coord(neighbours_coordinates[i_direction]) == None:
                            candidate_free_face = i_direction
                            candidate_module_is_free = True
                            break
                    if candidate_module_is_free:
                        temp_configuration.modules.remove(candidate_module)
                        self.set_removal_order_index(self.module_at_coord((i_x, i_z)))
                        self.module_at_coord((i_x, i_z)).set_last_direction(manhattan_neighbourhood.index(candidate_free_face))
                        # print('setting to ' + candidate_free_face)
                        break

    def get_time_to_reach_sink(self):
        # Get times it takes each module to reach the sink
        time_to_reach_sink = []
        temp_configuration = copy.deepcopy(self)
        for i_module in temp_configuration.__module_removal_order:
            time_taken = 0
            at_sink = False
            i_module.make_active()
            while not at_sink:
                at_sink = i_module.move(temp_configuration)
                time_taken = time_taken + 1
                if time_taken > 10000:
                    at_sink = True
            time_to_reach_sink.append(time_taken)
            temp_configuration.modules.remove(i_module)

        return time_to_reach_sink

    def get_parallel_module_activation_times(self):
        # Get times it takes each module to reach the sink
        time_to_reach_sink = self.get_time_to_reach_sink()
        # Calculate leaving times based on time to reach sink
        activation_times = [0]
        for i, i_time in enumerate(time_to_reach_sink[:-1]):
            activation_times.append(max(0, activation_times[i] + 2 + i_time - time_to_reach_sink[i + 1]))

        return activation_times

    def get_parallel_module_activation_times_collision_check(self):
        time_to_reach_sink = self.get_time_to_reach_sink()
        # Calculate initial leaving times
        activation_times = self.get_parallel_module_activation_times()
        # Simulate movement, checking for collisions
        for checking_module_index in range(1, len(activation_times)):
            successful_remove = False
            while not successful_remove:
                checking_time_to_reach_sink = time_to_reach_sink[:checking_module_index+1]
                checking_activation_times = activation_times[:checking_module_index+1]
                checking_arrival_times = np.array(checking_activation_times) + np.array(checking_time_to_reach_sink)
                temp_configuration = copy.deepcopy(self)
                checking_module = temp_configuration.__module_removal_order[checking_module_index]
                for i, i_arrival_time in enumerate(checking_arrival_times):
                    if i_arrival_time < checking_activation_times[-1]:
                        temp_configuration.modules.remove(temp_configuration.__module_removal_order[i])
                        checking_activation_times[i] = 100000
                current_time = 0
                collision = False
                checking_module_at_sink = False
                module_to_remove = None
                temp_module_list = temp_configuration.__module_removal_order[:checking_module_index+1]
                while not collision and not checking_module_at_sink:
                    if current_time in checking_activation_times:
                        i_module_activation_indexes = [i for i, x in enumerate(checking_activation_times) if x == current_time]
                        for i_module_activation_index in i_module_activation_indexes:
                            temp_configuration.__module_removal_order[i_module_activation_index].make_active()
                    for i_module in temp_module_list:
                        at_sink = False
                        if i_module.is_active():
                            # Move module
                            at_sink = i_module.move(temp_configuration)
                            if at_sink:
                                if i_module == checking_module:
                                    checking_module_at_sink = True
                                module_to_remove = i_module
                            # Check no neighbours are active modules
                            i_module_neighbour_coords = i_module.get_neighbour_coordinates()
                            for i_direction in moore_neighbourhood:
                                neighbour_module = temp_configuration.module_at_coord(i_module_neighbour_coords[i_direction])
                                if neighbour_module != None and neighbour_module.is_active():
                                    collision = True
                                    break
                    if module_to_remove:
                        temp_configuration.modules.remove(module_to_remove)
                        temp_module_list.remove(module_to_remove)
                        module_to_remove = None
                    current_time = current_time + 1
                if collision:
                    for i_module_index in range(checking_module_index, len(activation_times)):
                        activation_times[i_module_index] = activation_times[i_module_index] + 1
                elif checking_module_at_sink:
                    successful_remove = True

        return activation_times

    def get_sequential_module_activation_times(self):
        # Get times it takes each module to reach the sink
        time_to_reach_sink = self.get_time_to_reach_sink()
        # Calculate leaving times based on time to reach sink
        activation_times = [0]
        for i, i_time in enumerate(time_to_reach_sink[:-1]):
            activation_times.append(activation_times[i] + time_to_reach_sink[i])

        return activation_times

    def remove_excluded_modules(self, activation_times):
        current_time = 0
        excluded_modules = True
        module_to_remove = None
        i_module_matrix, i_orderi_matrix = self.configuration_to_matrix()
        self.__reconfiguration_sequence.append((i_module_matrix, i_orderi_matrix))
        while excluded_modules:
            excluded_modules = False
            if current_time in activation_times:
                i_module_activation_indexes = [i for i, x in enumerate(activation_times) if x == current_time]
                for i_module_activation_index in i_module_activation_indexes:
                    self.__module_removal_order[i_module_activation_index].make_active()
            for i_module in self.modules:
                if i_module.get_type() >= 1:
                    excluded_modules = True
                if i_module.is_active():
                    at_sink = i_module.move(self)
                    if at_sink:
                        module_to_remove = i_module
            i_module_matrix, i_orderi_matrix = self.configuration_to_matrix()
            self.__reconfiguration_sequence.append((i_module_matrix, i_orderi_matrix))
            if module_to_remove:
                self.modules.remove(module_to_remove)
                module_to_remove = None
            current_time = current_time + 1
            if current_time > 10000:
                return None
